﻿using System.Collections.ObjectModel;
using System.Windows;

namespace WpfApp1
{
    /// <summary>
    /// ListBox.xaml の相互作用ロジック
    /// </summary>
    public partial class ListBox : Window
    {
        private ObservableCollection<Dto> _dtos = new ObservableCollection<Dto>();

        public ListBox()
        {
            InitializeComponent();

            _dtos.Add(new Dto("Images/delete.png", "delete"));
            _dtos.Add(new Dto("Images/done.png", "done"));
            _dtos.Add(new Dto("Images/home.png", "home"));

            MyListBox.ItemsSource = _dtos;
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            if (SingleRadioButton.IsChecked.Value)
            {
                MyListBox.SelectionMode = System.Windows.Controls.SelectionMode.Single;
            }
            else if (ExtendedeRadioButton.IsChecked.Value)
            {
                MyListBox.SelectionMode = System.Windows.Controls.SelectionMode.Extended;
            }
            else if (MultipleRadioButton.IsChecked.Value)
            {
                MyListBox.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
        }
    }

    public sealed class Dto
    {
        public Dto(string fileName, string name)
        {
            FileName = fileName;
            Name = name;
        }

        public string FileName { get; set; }
        public string Name { get; set; }
    }
}