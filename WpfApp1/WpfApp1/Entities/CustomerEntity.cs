﻿using SQLite;

namespace WpfApp1.Entities
{
    [Table("Customers")]
    public class CustomerEntity
    {
        public CustomerEntity()
        {
        }

        public CustomerEntity(string name)
        {
            Name = name;
        }

        public CustomerEntity(int id, string name)
        {
            Id = id;
            Name = name;
        }

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        /// <summary>
        /// 名前
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 電話番号
        /// </summary>
        public string Phone { get; set; }

        public override string ToString()
        {
            return $"{Id} - {Name} - {Phone}";
        }
    }
}