﻿using System.Windows;
using SQLite;
using WpfApp1.Entities;

namespace WpfApp1
{
    /// <summary>
    /// SQLiteSample.xaml の相互作用ロジック
    /// </summary>
    public partial class SQLiteSample : Window
    {
        public SQLiteSample()
        {
            InitializeComponent();
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            CustomerEntity customer = new CustomerEntity()
            {
                Name = NameTextBox.Text,
                Phone = PhoneTextBox.Text,
            };

            using (var connection = new SQLiteConnection(App.DatabasePath))
            {
                connection.CreateTable<CustomerEntity>();
                connection.Insert(customer);
            }
        }

        private void ReadButton_Click(object sender, RoutedEventArgs e)
        {
            using (var connection = new SQLiteConnection(App.DatabasePath))
            {
                connection.CreateTable<CustomerEntity>();
                var customers = connection.Table<CustomerEntity>().ToList();
            }
        }
    }
}