﻿using System.Windows;

namespace WpfApp1
{
    /// <summary>
    /// TabControl.xaml の相互作用ロジック
    /// </summary>
    public partial class TabControl : Window
    {
        public TabControl()
        {
            InitializeComponent();

            MyTabControl.SelectedIndex = 1;
        }
    }
}