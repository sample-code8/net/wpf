﻿using System;
using System.Windows;

namespace WpfApp1
{
    /// <summary>
    /// CheckBox.xaml の相互作用ロジック
    /// </summary>
    public partial class CheckBox : Window
    {
        public CheckBox()
        {
            InitializeComponent();
        }

        private void MyCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("MyCheckBox_Checked" + MyCheckBox.IsChecked);
        }

        private void MyCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("MyCheckBox_Checked" + MyCheckBox.IsChecked);
        }

        private void MyCheckBox_Indeterminate(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("MyCheckBox_Checked" + MyCheckBox.IsChecked);
        }
    }
}