﻿using System.Windows;
using SQLite;
using WpfApp1.Entities;

namespace WpfApp1
{
    /// <summary>
    /// SaveWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class SaveWindow : Window
    {
        private CustomerEntity _customer;

        public SaveWindow(CustomerEntity customer)
        {
            InitializeComponent();

            _customer = customer;

            if (customer != null)
            {
                this.NameTextBox.Text = customer.Name;
            }
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            if (NameTextBox.Text.Length < 1)
            {
                MessageBox.Show("名前を入力してください。");
                return;
            }

            using (var connection = new SQLiteConnection(App.DatabasePath))
            {
                connection.CreateTable<CustomerEntity>();
                if (_customer == null)
                {
                    connection.Insert(new CustomerEntity(NameTextBox.Text));
                }
                else
                {
                    connection.Update(new CustomerEntity(_customer.Id, NameTextBox.Text));
                }
                Close();
            }
        }
    }
}