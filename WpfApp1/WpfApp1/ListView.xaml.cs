﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using WpfApp1.Entities;

namespace WpfApp1
{
    /// <summary>
    /// ListView.xaml の相互作用ロジック
    /// </summary>
    public partial class ListView : Window
    {
        private ObservableCollection<CustomerEntity> _customers = new ObservableCollection<CustomerEntity>();
        private int _index = 0;

        public ListView()
        {
            InitializeComponent();

            AddCustomers(20);

            CustomerListView.ItemsSource = _customers;
        }

        private void AddCustomer()
        {
            AddCustomers(1);
        }

        private void AddCustomers(int count)
        {
            for (int i = 0; i < count; i++)
            {
                _customers.Add(new CustomerEntity { Id = ++_index, Name = $"Name{_index}", Phone = $"Phone{_index}" });
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            AddCustomer();
        }

        private void SearchTextBox_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            var filterList = _customers.Where(c => c.Name.Contains(SearchTextBox.Text)).ToList();
            CustomerListView.ItemsSource = filterList;
        }
    }
}