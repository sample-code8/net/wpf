﻿using System.Windows;

namespace WpfApp1
{
    /// <summary>
    /// EventSample.xaml の相互作用ロジック
    /// </summary>
    public partial class EventSample : Window
    {
        public EventSample()
        {
            InitializeComponent();
        }

        private void AButton_Click(object sender, RoutedEventArgs e)
        {
            ResultLabel.Content += "A";
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            ResultLabel.Content = string.Empty;
        }
    }
}