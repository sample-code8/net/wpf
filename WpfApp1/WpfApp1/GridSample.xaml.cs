﻿using System.Windows;

namespace WpfApp1
{
    /// <summary>
    /// GridSample.xaml の相互作用ロジック
    /// </summary>
    public partial class GridSample : Window
    {
        public GridSample()
        {
            InitializeComponent();
            ResultLabel.Content = "12345";
        }
    }
}