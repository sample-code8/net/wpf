﻿using System.Windows;
using SQLite;
using WpfApp1.Entities;

namespace WpfApp1
{
    /// <summary>
    /// ListView2.xaml の相互作用ロジック
    /// </summary>
    public partial class ListView2 : Window
    {
        public ListView2()
        {
            InitializeComponent();

            ReadDataBase();
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            var sw = new SaveWindow(null);
            sw.ShowDialog();
            ReadDataBase();
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            var selectedCustomer = CustomerListView.SelectedItem as CustomerEntity;
            if (selectedCustomer == null)
            {
                MessageBox.Show("行を選択してください。");
                return;
            }

            var sw = new SaveWindow(selectedCustomer);
            sw.ShowDialog();
            ReadDataBase();
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            var selectedCustomer = CustomerListView.SelectedItem as CustomerEntity;
            if (selectedCustomer == null)
            {
                MessageBox.Show("行を選択してください。");
                return;
            }

            using (var connection = new SQLiteConnection(App.DatabasePath))
            {
                connection.CreateTable<CustomerEntity>();
                connection.Delete(selectedCustomer);
            }
            ReadDataBase();
        }

        private void ReadDataBase()
        {
            using (var connection = new SQLiteConnection(App.DatabasePath))
            {
                connection.CreateTable<CustomerEntity>();
                CustomerListView.ItemsSource = connection.Table<CustomerEntity>().ToList();
            }
        }
    }
}