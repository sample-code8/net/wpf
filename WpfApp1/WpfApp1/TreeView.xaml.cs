﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;

namespace WpfApp1
{
    /// <summary>
    /// TreeView.xaml の相互作用ロジック
    /// </summary>
    public partial class TreeView : Window
    {
        private ObservableCollection<ListViewDto> _listViewDtos = new ObservableCollection<ListViewDto>();

        public TreeView()
        {
            InitializeComponent();

            var listViewDto1 = new ListViewDto("Name1");
            listViewDto1.ListViewDtos.Add(new ListViewDto("Name1-1"));
            listViewDto1.ListViewDtos.Add(new ListViewDto("Name1-2"));
            _listViewDtos.Add(listViewDto1);

            CTreeView.ItemsSource = _listViewDtos;
        }
    }

    public sealed class ListViewDto
    {
        public ListViewDto(string name)
        {
            Name = name;
        }

        public string Name { get; set; }
        public List<ListViewDto> ListViewDtos { get; set; } = new List<ListViewDto>();
    }
}