﻿using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace WpfApp1
{
    /// <summary>
    /// ProgressBar.xaml の相互作用ロジック
    /// </summary>
    public partial class ProgressBar : Window
    {
        private const string UNIT = "%";

        public ProgressBar()
        {
            InitializeComponent();
            ShowProgress();
        }

        private void ShowProgress()
        {
            ATextBlock.Text = AProguressBar.Value.ToString() + UNIT;
            BTextBlock.Text = BProguressBar.Value.ToString() + UNIT;
        }

        private void AProguressBar_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            ShowProgress();
        }

        private void AButton_Click(object sender, RoutedEventArgs e)
        {
            Task.Run(() =>
            {
                for (int i = 0; i < 10; i++)
                {
                    Thread.Sleep(500);
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        AProguressBar.Value += 10;
                    });
                }
            });
        }

        private void BProguressBar_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            ShowProgress();
        }

        private void BButton_Click(object sender, RoutedEventArgs e)
        {
            Task.Run(() =>
            {
                for (int i = 0; i < 10; i++)
                {
                    Thread.Sleep(500);
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        BProguressBar.Value += 10;
                    });
                }
            });
        }

        private void CButton_Click(object sender, RoutedEventArgs e)
        {
            CProguressBar.IsIndeterminate = true;
            CTextBlock.Text = "検索中";
        }
    }
}