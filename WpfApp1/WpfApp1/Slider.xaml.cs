﻿using System.Windows;

namespace WpfApp1
{
    /// <summary>
    /// Slider.xaml の相互作用ロジック
    /// </summary>
    public partial class Slider : Window
    {
        public Slider()
        {
            InitializeComponent();
        }

        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            ASlider.Text = e.NewValue.ToString();
        }
    }
}