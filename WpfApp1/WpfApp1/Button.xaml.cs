﻿using System;
using System.Windows;

namespace WpfApp1
{
    /// <summary>
    /// Button.xaml の相互作用ロジック
    /// </summary>
    public partial class Button : Window
    {
        public Button()
        {
            InitializeComponent();
        }

        private void NormalButton_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Normal Button Clicked!");
        }

        private void RepeatButton_Click(object sender, RoutedEventArgs e)
        {
            Console.WriteLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + " RepeatButton");
        }

        private void ToggleButton_Click(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("ToggleButton:" + ToggleButton.IsChecked);
        }
    }
}