﻿using System;
using System.IO;
using System.Windows;

namespace WpfApp1
{
    /// <summary>
    /// App.xaml の相互作用ロジック
    /// </summary>
    public partial class App : Application
    {
        private static string databaseName = "Shop.db";
        private static string folderPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
        public static string DatabasePath = Path.Combine(folderPath, databaseName);
    }
}